import torch as tr
import numpy as np
from neural_wrappers.pytorch import NWModule, device
from neural_wrappers.pytorch import trGetData, npGetData
from overrides import overrides
from tqdm import tqdm
from copy import deepcopy

from .model import GeneratorFullModel, DiscriminatorFullModel
from .util import normalize_kp

class FirstOrder(NWModule):
	def __init__(self, modelCfg, trainCfg):
		super().__init__(hyperParameters={"modelCfg" : modelCfg, "trainCfg" : trainCfg})
		from .generator import OcclusionAwareGenerator
		from .discriminator import MultiScaleDiscriminator
		from .keypoint_detector import KPDetector
		from .vgg19 import Vgg19

		self.modelCfg = modelCfg
		self.trainCfg = trainCfg
		self.generator = OcclusionAwareGenerator(**modelCfg["generator_params"], **modelCfg["common_params"])
		self.discriminator = MultiScaleDiscriminator(**modelCfg["discriminator_params"], **modelCfg["common_params"])
		self.kp_detector = KPDetector(**modelCfg["kp_detector_params"], **modelCfg["common_params"])
		self.perceptualType = Vgg19
		self.generator_full = GeneratorFullModel(self.kp_detector, self.generator, \
			self.discriminator, self.perceptualType, trainCfg)
		self.discriminator_full = DiscriminatorFullModel(self.kp_detector, self.generator, self.discriminator, trainCfg)
		self.setCriterion(lambda x : x)

	@overrides
	def networkAlgorithm(self, npInputs, npLabels, isTraining=False, isOptimizing=False):
		trInputs, trLabels = trGetData(npInputs), trGetData(npLabels)

		losses_generator, generated = self.generator_full(trInputs)
		loss_values = [val.mean() for val in losses_generator.values()]
		loss = sum(loss_values)

		if isTraining and isOptimizing:
			loss.backward()
			self.optimizer.optimizers["generator"].step()
			self.optimizer.optimizers["generator"].zero_grad()
			self.optimizer.optimizers["kp_detector"].step()
			self.optimizer.optimizers["kp_detector"].zero_grad()
		else:
			loss.detach_()

		if self.trainCfg["loss_weights"]["generator_gan"] != 0:
			if isTraining and isOptimizing:
				self.optimizer.optimizers["discriminator"].zero_grad()
			losses_discriminator = self.discriminator_full(trInputs, generated)
			loss_values = [val.mean() for val in losses_discriminator.values()]
			loss = sum(loss_values)

			if isTraining and isOptimizing:
				loss.backward()
				self.optimizer.optimizers["discriminator"].step()
				self.optimizer.optimizers["discriminator"].zero_grad()
			else:
				loss.detach_()
		else:
			losses_discriminator = {}

		losses_generator.update(losses_discriminator)
		trResults = {"generated" : generated}
		trLoss = {"generator" : losses_generator, "discriminator" : losses_discriminator}

		npLossGenerator = npGetData({key: value.mean().detach().cpu().numpy() for \
			key, value in trLoss["generator"].items()})
		npLossDiscriminator = npGetData({key: value.mean().detach().cpu().numpy() for \
			key, value in trLoss["discriminator"].items()})
		npResults = npGetData(trResults)
		npLoss = {"generator" : npLossGenerator, "discriminator" : npLossDiscriminator}
		# TODO: actual multi-netowrk/GAN loss, similarily like for Graph.
		npLoss = sum(tuple(npLoss["generator"].values()))
		return npResults, npLoss

	def loadOriginalWeights(self, path):
		print("FirstOrderModel::loadOriginalWeights] Loading weights from %s" % (path))
		checkpoint = tr.load(path, map_location=device)	 
		self.generator.load_state_dict(checkpoint["generator"])
		self.kp_detector.load_state_dict(checkpoint["kp_detector"])
		self.discriminator.load_state_dict(checkpoint["discriminator"])

	# def npForwardDemo(self, source_frame, driving_video):
	# 	result = np.zeros(driving_video.shape)
	# 	source_frame = np.expand_dims(source_frame.transpose(2, 0, 1), axis=0).astype(np.float32)
	# 	initial_driving_frame = driving_video[0 : 1].transpose(0, 3, 1, 2).astype(np.float32)
	# 	nFrames = len(driving_video)

	# 	prevState = self.train if self.training else self.eval
	# 	self.eval()
	# 	kp_source = self.kp_detector.npForward(source_frame)
	# 	kp_driving_initial = self.kp_detector.npForward(initial_driving_frame)
	# 	for i in tqdm(range(nFrames)):
	# 		driving_frame = driving_video[i : i + 1].transpose(0, 3, 1, 2).astype(np.float32)
	# 		kp_driving = self.kp_detector.npForward(driving_frame)
	# 		kp_norm = normalize_kp(kp_source=kp_source, kp_driving=kp_driving,
	# 			kp_driving_initial=kp_driving_initial, use_relative_movement=True,
	# 			use_relative_jacobian=True, adapt_movement_scale=True)

	# 		out = self.generator.npForward(source_frame, kp_source=kp_source, kp_driving=kp_norm)
	# 		npOut = out["prediction"][0].transpose(1, 2, 0)
	# 		result[i] = npOut
	# 	prevState()
	#	return result