from neural_wrappers.callbacks import Callback
from pathlib import Path
import numpy as np

class SaveNPY(Callback):
	def __init__(self, dir):
		super().__init__()
		self.dir = dir
		Path(dir).mkdir(parents=True, exist_ok=True)
		self.iter = 0

	def onEpochStart(self, **kwargs):
		self.iter = 0

	def onIterationEnd(self, results, labels, **kwargs):
		y = np.int32(results["generated"]["prediction"] * 255)
		t = np.int32(labels["driving"] * 255)
		diffs = np.abs(y - t)
		for j in range(len(y)):
			np.save("%s/%d.npy" % (self.dir, self.iter), y[j])
			self.iter += 1

def export_npy(model, generator, numSteps, dir):
    model.addCallbacks([SaveNPY(dir)])
    res = model.test_generator(generator, numSteps, printMessage="v2")
    print(res)
