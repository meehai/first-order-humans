import numpy as np
from neural_wrappers.readers import DatasetReader
from neural_wrappers.utilities import fullPath
from media_processing_lib.video import tryReadVideo
from overrides import overrides
from typing import List

class FineTuneVideoReader(DatasetReader):
	def __init__(self, datasetPath:str, targetFrameIx:int=0):
		super().__init__(
			dataBuckets={"data" : ["rgb"]},
			dimGetter={"rgb" : FineTuneVideoReader.rgbGetter},
			dimTransform={}
		)

		self.datasetPath = fullPath(datasetPath)
		self.video = tryReadVideo(self.datasetPath, vidLib="pims")
		self.targetFrameIx = targetFrameIx
		self.targetFrame = self.video[targetFrameIx]
		self.datasetFormat.isCacheable = True

	@staticmethod
	def mergeFn(x, transform):
		MB = len(x)
		H, W = x[0]["data"]["rgb"]["source"].shape[0 : 2]
		batchSource, batchDriving = np.zeros((2, MB, 3, H, W), dtype=np.float32)
		for i in range(MB):
			source = x[i]["data"]["rgb"]["source"]
			driving = x[i]["data"]["rgb"]["driving"]
			source = np.float32(source) / 255
			driving = np.float32(driving) / 255
			if not transform is None:
				driving = transform([driving])[0]
			batchSource[i] = source.transpose(2, 0, 1)
			batchDriving[i] = driving.transpose(2, 0, 1)

		items = {
			"source" : batchSource,
			"driving" : batchDriving,
		}
		return items, items

	def rgbGetter(obj, index:int):
		source = obj.targetFrame
		driving = obj.video[index]
		return {"source" : source, "driving" : driving, "path" : obj.datasetPath, \
			"indexes" : [obj.targetFrameIx, index]}

	@overrides
	def getDataset(self):
		return self

	@overrides
	def __len__(self) -> int:
		return len(self.video)

	def __str__(self) -> str:
		Str = super().__str__()
		Str += "\n[VideoDirReader]"
		Str += "\n - Dataset path: %s" % self.datasetPath
		Str += "\n - Video shape: %s" % str(self.video.shape)
		Str += "\n - Target frame ix: %d" % self.targetFrameIx
		return Str