import numpy as np
import yaml
import torch as tr
from argparse import ArgumentParser
from pathlib import Path
# from sync_batchnorm import DataParallelWithCallback
from neural_wrappers.pytorch import device
from neural_wrappers.utilities import changeDirectory, fullPath

from models import getModel
from readers import getReader

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("--dataCfgPath", required=True)

	parser.add_argument("--modelCfgPath")
	parser.add_argument("--trainCfgPath")
	parser.add_argument("--weightsFile")
	parser.add_argument("--dir")

	args = parser.parse_args()
	args.dataCfg = yaml.load(open(args.dataCfgPath, "r"), Loader=yaml.SafeLoader)
	args.dataCfg["datasetPath"] = fullPath(args.dataCfg["datasetPath"])
	assert args.dataCfg["datasetPath"].exists(), "Unknown dataset path: '%s' from data cfg: '%s'" % \
		(args.dataCfg["datasetPath"], args.dataCfgPath)
	# args.deviceIDs = list(map(lambda x : int(x), args.deviceIDs.split(",")))

	if args.type in ("test", "retrain", "train_pretrained"):
		assert not args.weightsFile is None
		args.weightsFile = fullPath(args.weightsFile)
		Model = tr.load(args.weightsFile)
		Weights, State = Model["weights"], Model["model_state"]
		args.modelCfg = State["modelCfg"]
		args.trainCfg = State["trainCfg"]
	else:
		assert not args.modelCfgPath is None
		assert not args.trainCfgPath is None
		args.modelCfg = yaml.load(open(args.modelCfgPath, "r"), Loader=yaml.SafeLoader)
		args.trainCfg = yaml.load(open(args.trainCfgPath, "r"), Loader=yaml.SafeLoader)

	return args

def main():
	args = getArgs()
	reader = getReader(args.dataCfg, args.trainCfg)
	g = reader.iterate()
	print(reader)

	model = getModel(args.modelCfg, args.trainCfg).to(device)

	if args.type == "train":
		changeDirectory(args.dir, expectExist=False)
		print(model.summary())
		model.trainGenerator(g, args.trainCfg["num_epochs"])
	if args.type == "train_pretrained":
		changeDirectory(args.dir, expectExist=False)
		model.loadWeights(args.weightsFile)
		for param in model.kp_detector.parameters():
			param.requires_grad = False
		model.currentEpoch = 1
		print(model.summary())
		model.trainGenerator(g, args.trainCfg["num_epochs"])
	elif args.type == "retrain":
		changeDirectory(args.dir, expectExist=True)
		model.loadModel(args.weightsFile)
		print(model.summary())
		model.trainGenerator(g, args.trainCfg["num_epochs"])
	elif args.type == "test":
		model.loadWeights(args.weightsFile)
		model.eval()
		print(model.summary())
		res = model.testGenerator(g)
		print(res["Test"])
	else:
		assert False

if __name__ == "__main__":
	main()
